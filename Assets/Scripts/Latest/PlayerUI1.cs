﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using System;

public class PlayerUI1 : NetworkBehaviour {


    [SyncVar]
    public uint score = 0;

    public Dictionary<uint, Image> lstIds; // Image : gauge santé
    public Dictionary<uint, Text> lstIdtext; // Text netID
    public Dictionary<uint, Text> lstIdscore; // score
    public Image weaponGauge;

    public override void OnStartLocalPlayer()
    {
        if (!hasAuthority)
            return;
        //id = netId.Value;


        if(isServer)
        {
            GameObject go = (GameObject)Instantiate(Resources.Load("Score Handler", typeof(GameObject)) as GameObject,
                                                    transform.position, transform.rotation);
            NetworkServer.Spawn(go);
        }

        lstIds = new Dictionary<uint, Image>();
        lstIdtext = new Dictionary<uint, Text>();
        lstIdscore = new Dictionary<uint, Text>();
        if(isClient)
        {
            AddUI(netId.Value);
            CmdNotifyIDToServer(netId.Value);
            CmdNotifyToScoreManager(netId.Value);
        }

    }

    [ClientCallback]
    void Start()
    {
        //if (isLocalPlayer)
        //    CmdNotifyIDToServer(netId.Value);
    }


    [ClientCallback]
    void Update () {
        if (!isLocalPlayer)
            return;

        if (weaponGauge.fillAmount < 1.0f)
            weaponGauge.fillAmount += Time.deltaTime * 0.3f;

    }



    [Command]
    void CmdNotifyToScoreManager(uint id)
    {
        GameObject.Find("Score Handler(Clone)").GetComponent<ScoreHandler>().NotifyIDToScoreManager(id);
    }




    // Commande pour ajouter aux clients des elements ui quand un joueur se connecte
    [Command]
    void CmdNotifyIDToServer(uint id)
    {
        //if (!hasAuthority || !isLocalPlayer)
        //if(!isClient || !isLocalPlayer)
        //    return;


        var lstPlayers = GameObject.FindGameObjectsWithTag("Player");

        foreach (var go in lstPlayers)
        {

            uint uid = go.GetComponent<NetworkIdentity>().netId.Value;

            if (id != uid)
            {
                RpcAddToLists(uid);
                go.GetComponent<PlayerUI1>().RpcAddToLists(id);
            }
           
        }
    }

    // called depusi le serveur pour dire aux client de mettre
    // à jour leur UI quand un joueur se co
    [ClientRpc]
    void RpcAddToLists(uint p_id)
    {
        if (!isClient)
            return;

        if (isLocalPlayer && !lstIds.ContainsKey(p_id))
        {
            AddUI(p_id);
        }
    }




    [ClientCallback]
    void AddUI(uint p_id)
    {
        int nb = lstIds.Count;


        GameObject go = new GameObject();
        go.transform.parent = transform.FindChild("Main Camera/JoueurCanvas").transform;
        Image img = go.AddComponent<Image>();

        img.rectTransform.localPosition = new Vector3(-Screen.width / 2 + 100, Screen.height / 4 - nb * 30, 0);
        img.rectTransform.localScale = new Vector3(1, 1, 1);
        img.rectTransform.localRotation = Quaternion.Euler(0, 0, 0);
        img.rectTransform.sizeDelta = new Vector2(100, 10);
#if UNITY_EDITOR
        //img.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Background.psd");
        //img.sprite = Resources.GetBuiltinResource(typeof(Sprite), "Resources/unity_builtin_extra/Background") as Sprite;
#endif
        lstIds.Add(p_id, img);

        GameObject go1 = new GameObject();
        go1.transform.parent = transform.FindChild("Main Camera/JoueurCanvas").transform;
        Text txt = go1.AddComponent<Text>();

        txt.text = "" + p_id;
        txt.rectTransform.localPosition = new Vector3(-Screen.width / 2 + 20, Screen.height / 4 - nb * 30, 0);
        txt.rectTransform.localScale = new Vector3(1, 1, 1);
        txt.rectTransform.localRotation = Quaternion.Euler(0, 0, 0);
        txt.rectTransform.sizeDelta = new Vector2(50, 30);
        txt.fontSize = 20;
        txt.alignment = TextAnchor.MiddleCenter;
        txt.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

        lstIdtext.Add(p_id, txt);


        GameObject go2 = new GameObject();
        go2.transform.parent = transform.FindChild("Main Camera/JoueurCanvas").transform;
        Text txt1 = go2.AddComponent<Text>();
        txt1.text = "" + 0;
        txt1.rectTransform.localPosition = new Vector3(-Screen.width / 2 + 160, Screen.height / 4 - nb * 30, 0);
        txt1.rectTransform.localScale = new Vector3(1, 1, 1);
        txt1.rectTransform.localRotation = Quaternion.Euler(0, 0, 0);
        txt1.rectTransform.sizeDelta = new Vector2(50, 30);
        txt1.fontSize = 20;
        txt1.alignment = TextAnchor.MiddleCenter;
        txt1.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

        lstIdscore.Add(p_id, txt1);

    }

    [ClientCallback]
    void RemoveFromLists(uint p_id)
    {

        Image img = lstIds[p_id];
        lstIds.Remove(p_id);
        Destroy(img.gameObject);

        Text txt = lstIdtext[p_id];
        lstIdtext.Remove(p_id);
        Destroy(txt.gameObject);

        Text txt1 = lstIdscore[p_id];
        lstIdscore.Remove(p_id);
        Destroy(txt1.gameObject);

    }

    [ClientRpc]
    public void RpcUpdateHealth(uint id, int health)
    {
        if(isLocalPlayer)
        {
            lstIds[id].rectTransform.sizeDelta = new Vector2(health, 10);
        }
    }

    [ClientRpc]
    public void RpcUpdateScore(uint id, uint score)
    {





        if (isLocalPlayer)
        {
            lstIdscore[id].text = ""+score;
        }
    }

    [ClientRpc]
    public void RpcUpdateAllHealth()
    {

        if (isLocalPlayer)
        {
            foreach (var key in lstIds.Keys.ToList())
                lstIds[key].rectTransform.sizeDelta = new Vector2(100, 10);
        }
    }


    public float GetAmountOfWeaponGauge()
    {
        return weaponGauge.fillAmount;
    }


    public void UpdateWeaponGauge(float v)
    {
        weaponGauge.fillAmount -= v;
    }


}
