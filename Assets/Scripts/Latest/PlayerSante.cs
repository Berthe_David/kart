﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using System;

public class PlayerSante : NetworkBehaviour {

    public const int maxHealth = 100;
    [SyncVar(hook ="OnHealth")] public int health = maxHealth;
    [SyncVar(hook = "OnAlive")] public bool isAlive = true;
    [SyncVar] public bool canDamaged = true;
    public int Health { get { return health; } }

    [ServerCallback]
    public void TakeDamage(int amount)
    {

        if (canDamaged)
        {
            health -= amount;
            if (health <= 0)
            {
                isAlive = false;
                CmdUpdateStatusAlive(netId.Value, isAlive);
            }
            StartCoroutine(StopDamaged());
        }


    }

    IEnumerator StopDamaged()
    {
        canDamaged = false;
        yield return new WaitForSeconds(2);
        canDamaged = true;
    }

    void OnHealth(int value)
    {


        health = value;


        //if(health <= 0)
        //isAlive = false; // Probléme exe only on client then not synch

    }

    void OnAlive(bool value)
    {
        isAlive = value;

    }


    [Command]
    void CmdUpdateStatusAlive(uint id, bool b)
    {

        GameObject.Find("Score Handler(Clone)").GetComponent<ScoreHandler>().UpdateStatusAlive(id, b);
    }


    //[ClientCallback]
    //void Update()
    //{

    //}


}
