﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.IO;
using System;

public class Missile : NetworkBehaviour
{

    [ServerCallback]
    void OnCollisionEnter(Collision collision)
    {

        //FileStream fs;
        //StreamWriter sw;
        //String path = "test.log";
        //if (!File.Exists(path))
        //    fs = new FileStream(path, FileMode.Create);
        //else
        //    fs = new FileStream(path, FileMode.Append);
        //sw = new StreamWriter(fs);
        //sw.WriteLine(DateTime.Now + " Missile::OnCollisionEnter - isClient : " + isClient + ", isServer : " + isServer + " : hasAutho : " + hasAuthority + ", id : " + netId.Value);
        //sw.Close();

        var hit = collision.gameObject;
        var health = hit.GetComponent<PlayerSante>();
        if (health != null)
        {
            health.TakeDamage(10);
            uint id = hit.GetComponent<NetworkIdentity>().netId.Value;
            var lst_players = GameObject.FindGameObjectsWithTag("Player");
            foreach (var go in lst_players)
            {
                go.GetComponent<PlayerUI1>().RpcUpdateHealth(id, health.Health);
            }
        }




        Destroy(gameObject);

    }


}
