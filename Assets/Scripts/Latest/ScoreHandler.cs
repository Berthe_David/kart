﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

public class ScoreHandler : NetworkBehaviour {

    Dictionary<uint, uint> dicoPlayersScore;
    Dictionary<uint, bool> dicoPlayersStatus;

    [ServerCallback]
    void Awake()
    {
        //if (isServer)
        //{
            if (dicoPlayersScore == null)
                dicoPlayersScore = new Dictionary<uint, uint>();
            if (dicoPlayersStatus == null)
                dicoPlayersStatus = new Dictionary<uint, bool>();
        //StartCoroutine(LoopNumPlayers());
        //}

        //FileStream fs;
        //StreamWriter sw;
        //String path = "test.log";
        //if (!File.Exists(path))
        //    fs = new FileStream(path, FileMode.Create);
        //else
        //    fs = new FileStream(path, FileMode.Append);
        //sw = new StreamWriter(fs);
        //sw.WriteLine(DateTime.Now + " - ScoreHandler::Awake - isClient : " + isClient + ", isServer : " + isServer + " - hasAutho : " + hasAuthority + " - isLocal : " + isLocalPlayer);
        //sw.Close();

    }

    // pas necessare je pense
    //[Command]
    [ServerCallback]
    public void NotifyIDToScoreManager(uint p_id)
    {

        if (!dicoPlayersScore.ContainsKey(p_id))
        {
            dicoPlayersScore.Add(p_id, 0);
        }

        if (!dicoPlayersStatus.ContainsKey(p_id))
        {
            dicoPlayersStatus.Add(p_id, true);
        }

        //FileStream fs;
        //StreamWriter sw;
        //String path = "test.log";
        //if (!File.Exists(path))
        //    fs = new FileStream(path, FileMode.Create);
        //else
        //    fs = new FileStream(path, FileMode.Append);
        //sw = new StreamWriter(fs);
        //sw.WriteLine(DateTime.Now + " - ScoreHandler::CmdNotifySM - NB : "+dicoPlayersScore.Count);

        //foreach(var it in dicoPlayersScore)
        //    sw.WriteLine("id " + it.Key + " : " + it.Value + " -- " + dicoPlayersStatus[it.Key]);

        //sw.Close();

    }

    [ServerCallback]
    public void UpdateStatusAlive(uint id, bool b)
    {

        //FileStream fs;
        //StreamWriter sw;
        //String path = "test.log";
        //if (!File.Exists(path))
        //    fs = new FileStream(path, FileMode.Create);
        //else
        //    fs = new FileStream(path, FileMode.Append);
        //sw = new StreamWriter(fs);
        //sw.WriteLine(DateTime.Now + " ScoreHandler::UpdateStatusAlive B - id " + netId + ", alive : " + b + " - isServer : " + isServer + ", isClient : " + isClient + " - hasAutho : " + hasAuthority+" - isLocal : " + isLocalPlayer);
        //sw.Close();



        dicoPlayersStatus[id] = b;
        CheckOneAlive();

    }
    [ServerCallback]
    void CheckOneAlive()
    {



        //FileStream fs;
        //StreamWriter sw;
        //String path = "test.log";
        //if (!File.Exists(path))
        //    fs = new FileStream(path, FileMode.Create);
        //else
        //    fs = new FileStream(path, FileMode.Append);
        //sw = new StreamWriter(fs);
        //foreach (var item in dicoPlayersStatus)
        //    sw.WriteLine(DateTime.Now + " CheckOneAlive - key : ("+item.Key+", "+item.Value+") - id " + netId + " - isServer : " + isServer + ", isClient : " + isClient + " - hasAutho : " + hasAuthority+ " - isLocal : " + isLocalPlayer);
        //sw.Close();




        int i = 0;
        uint id = 0;
        foreach (var item in dicoPlayersStatus)
        {
            if (item.Value)
            {
                i++;
                id = item.Key;
            }
        }

        if(i == 1)
        {
            dicoPlayersScore[id]++;

            var lst_players = GameObject.FindGameObjectsWithTag("Player");
            var spawns = FindObjectsOfType<NetworkStartPosition>().ToList();
            int nbSpawns = spawns.Count;
            int rd = UnityEngine.Random.Range(0, nbSpawns);
            //bool flag = false;
            //var lst = new List<int>();
            //lst.Add(rd);
            foreach (var go in lst_players)
            {
                go.GetComponent<PlayerSante>().health = 100;
                go.GetComponent<PlayerSante>().isAlive = true;
                go.GetComponent<PlayerUI1>().RpcUpdateScore(id, dicoPlayersScore[id]);
                go.GetComponent<PlayerUI1>().RpcUpdateAllHealth();
                go.GetComponent<Rigidbody>().Sleep();
                go.GetComponent<PlayerCarMvt>().RpcResetGame(spawns[rd].transform.position);

                spawns.RemoveAt(rd);
                nbSpawns = spawns.Count;
                rd = UnityEngine.Random.Range(0, nbSpawns);
                //if (flag)
                //{
                //    int rd_bis = UnityEngine.Random.Range(0, 2);
                //    while (lst.Contains(rd_bis))
                //    {
                //        rd_bis = UnityEngine.Random.Range(0, 2);
                //    }
                //    lst.Add(rd_bis);

                //}

                //flag = true;

                //go.GetComponent<PlayerCarMvt>().RpcResetGame();


            }

            //ResetGame();
        }


        //FileStream fs1;
        //StreamWriter sw1;
        //String path1 = "test.log";
        //if (!File.Exists(path1))
        //    fs1 = new FileStream(path1, FileMode.Create);
        //else
        //    fs1 = new FileStream(path1, FileMode.Append);
        //sw1 = new StreamWriter(fs1);
        //var list_players = GameObject.FindGameObjectsWithTag("Player");
        //foreach (var go in list_players)
        //{
        //int santee = go.GetComponent<PlayerSante>().health;
        //bool alive = go.GetComponent<PlayerSante>().isAlive;
        //sw1.WriteLine(DateTime.Now + " CheckOneAlive Santé : "+santee+"/"+alive+
        //    " - id " + netId+ " - isServer : " + isServer + ", isClient : " + isClient + " - hasAutho : " + hasAuthority + " - isLocal : " + isLocalPlayer);

        //}
        //sw1.Close();

        foreach (var key in dicoPlayersStatus.Keys.ToList())
        {
            dicoPlayersStatus[key] = true;
        }

    }




}
