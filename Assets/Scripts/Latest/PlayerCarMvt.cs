﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

public class PlayerCarMvt : NetworkBehaviour
{

    public float h, v, last_v = 0;
    public bool doJump = false, doFire = false;
    const float MAX_DRAG = 2.5f;
    const float MIN_DRAG = 0.1f;
    const float SMOOTH_DRAG = 0.05f;
    const float MAX_ANG_DRAG = 15.0f;
    const float MIN_ANG_DRAG = 10.0f;
    const float SMOOTH_ANG_DRAG = 0.05f;
    public float MaxVelo = 20;
    public float MaxAngVelo = 20;

    public float speed = 30;
    public float reverseSpeed = 30;
    public float turnSpeed = 20;

    public bool canMove = true;
    public bool canShoot = true;
    public bool isGround;
    public float forceJump = 10;

    public int maxAngleGround = 60;
    public float angle;

    float maxAngularDrag = MAX_ANG_DRAG;
    float currentAngularDrag = MIN_ANG_DRAG;

    float maxDrag = MIN_DRAG;
    float currentDrag = MAX_DRAG;

    public GameObject bulletPrefab;
    public Transform bulletSpawn;


    public float delayShoot = 0.1f;
    public float currentDelayShoot = 0f;

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.yellow;
    }


    [ClientCallback]
    void Start()
    {
        if (!isLocalPlayer)
        {
            transform.FindChild("Main Camera/JoueurCanvas").gameObject.SetActive(false);
            GetComponentInChildren<Camera>().enabled = false;
            return;
        }
        StartCoroutine(ResetPosRota());
    }

    [ClientCallback]
    void Update()
    {
        if (isLocalPlayer)
        {
            //    return;

            h = Input.GetAxisRaw("Horizontal");
            v = Input.GetAxisRaw("Vertical");
            if (Input.GetButtonDown("Fire1"))
                doFire = true;
            if (Input.GetKeyDown(KeyCode.Space))
                doJump = true;
            //currentDelayShoot += Time.deltaTime;
        }
    }

    [ClientCallback]
    void FixedUpdate()
    {


        if (isLocalPlayer)
        {
            CmdMovement(h, v);

            if (doFire /*&& currentDelayShoot >= delayShoot*/)
            {
                doFire = false;
                float value = GetComponent<PlayerUI1>().GetAmountOfWeaponGauge();
                if (value > 0.2f)
                {
                    CmdFire();
                    //currentDelayShoot = 0.0f;
                    GetComponent<PlayerUI1>().UpdateWeaponGauge(0.2f);
                }
            }

            if (doJump)
            {
                doJump = false;
                CmdJump();
            }

        }
    }

    
    void CmdJump()
    {
        if (isGround)
            GetComponent<Rigidbody>().AddForce(Vector3.up * forceJump, ForceMode.Impulse);
    }

    [Command]
    void CmdFire()
    {
        //if (!canShoot)
        //    return;
        //// Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position + bulletSpawn.forward * 2,
            bulletSpawn.rotation);

        //// Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 30;
        NetworkServer.Spawn(bullet);
        //// Destroy the bullet after 2 seconds
        Destroy(bullet, 4.0f);
        
    }


    
    void CmdMovement(float hh, float vv)
    {
        if (!canMove)
            return;

        Rigidbody rb = GetComponent<Rigidbody>();
        float currentSpeed = Mathf.Abs(transform.InverseTransformDirection(rb.velocity).z);
        float tmp_z = transform.InverseTransformDirection(rb.velocity).z;
        //float signedSpeed = tmp_z / Mathf.Abs(tmp_z);

        float angularDragLerpTime = currentSpeed * SMOOTH_ANG_DRAG;
        float dragLerpTime = currentSpeed * SMOOTH_DRAG;

        float myAngularDrag = Mathf.Lerp(currentAngularDrag, maxAngularDrag, angularDragLerpTime);
        float myDrag = Mathf.Lerp(currentDrag, maxDrag, dragLerpTime);

        rb.angularDrag = myAngularDrag;
        if (isGround)
        {
            rb.drag = myDrag;

            rb.angularDrag = myAngularDrag;
            //rb.mass = 1;
        }
        else
        {
            rb.drag = 0;

            //rb.angularDrag = 0;
            //rb.mass = 30;
        }

        float moveDirection;
        float turnDirection;

        if (vv > 0.0f)
        {
            moveDirection = vv * speed;
            rb.AddRelativeForce(0, 0, moveDirection, ForceMode.Force);
            last_v = vv;
        }
        else if (vv < 0.0f)
        {
            moveDirection = vv * reverseSpeed;
            rb.AddRelativeForce(0, 0, moveDirection, ForceMode.Force);
            last_v = vv;
        }

        if (currentSpeed > 0.05f)
        {
            turnDirection = last_v * hh * turnSpeed;
            rb.AddRelativeTorque(0, turnDirection, 0, ForceMode.Force);
        }

        if (currentSpeed > MaxVelo)
            rb.velocity = rb.velocity.normalized * MaxVelo;

    }


    [ClientCallback]
    IEnumerator ResetPosRota()
    {
        for (;;)
        {
            if (isGround)
            {
                Vector3 angles = transform.rotation.eulerAngles;
                if (angles.x > 60 && angles.x < 320)
                    transform.rotation = Quaternion.Euler(0, angles.y, angles.z);
                if (angles.z > 80 && angles.z < 280)
                    transform.rotation = Quaternion.Euler(angles.x, angles.y, 0);
            }
            yield return new WaitForSeconds(1.0f);
        }
    }

    [ClientCallback]
    void OnCollisionEnter(Collision collision)
    {

            Impact(collision.contacts[0].normal);
    }
    [ClientCallback]
    void OnCollisionStay(Collision collision)
    {
       
            Impact(collision.contacts[0].normal);
    }
    [ClientCallback]
    void OnCollisionExit(Collision collision)
    {
       
            isGround = false;
    }

    [ClientCallback]
    void Impact(Vector3 normal)
    {
        angle = Vector3.Angle(normal, Vector3.up);

        if (angle < maxAngleGround)
            isGround = true;
        else
            isGround = false;

    }

    [ClientRpc]
    public void RpcResetGame(Vector3 pos)
    {

        transform.position = pos;
    }






}
